package es.uvigo.esei.dai.hybridserver;

import javax.jws.WebService;

import es.uvigo.esei.dai.hybridserver.DAO.ServerPagesHTML;
import es.uvigo.esei.dai.hybridserver.DAO.ServerPagesXML;
import es.uvigo.esei.dai.hybridserver.DAO.ServerPagesXSD;
import es.uvigo.esei.dai.hybridserver.DAO.ServerPagesXSLT;

@WebService(serviceName="HybridServerService", endpointInterface = "es.uvigo.esei.dai.hybridserver.ServerSource",
targetNamespace="http://hybridserver.dai.esei.uvigo.es/")
public class HybridServerService implements ServerSource{

	protected String url;
	protected String user;
	protected String password;
	
	public HybridServerService(String url,String user,String password) {
		this.url = url;
		this.user = user;
		this.password = password;
	}
	
	@Override
	public String getHTMLList() {
		
		StringBuilder t = new StringBuilder();
		
		for (String e : new ServerPagesHTML(url,user,password).allPages()) {
			t.append("<a href=\"html?uuid=" + e + "\">" + e + "</a><br>");			
		}
		return t.toString();
	}

	@Override
	public String getXMLList() {
		StringBuilder t = new StringBuilder();
		
		for (String e : new ServerPagesXML(url,user,password).allPages()) {
			t.append("<a href=\"xml?uuid=" + e + "\">" + e + "</a><br>");			
		}
		return t.toString();
	}

	@Override
	public String getXSDList() {
		StringBuilder t = new StringBuilder();
		
		for (String e : new ServerPagesXSD(url,user,password).allPages()) {
			t.append("<a href=\"xsd?uuid=" + e + "\">" + e + "</a><br>");			
		}
		return t.toString();
	}

	@Override
	public String getXSLTList() {
		StringBuilder t = new StringBuilder();
		
		for (String e : new ServerPagesXSLT(url,user,password).allPages()) {
			t.append("<a href=\"xslt?uuid=" + e + "\">" + e + "</a><br>");			
		}
		return t.toString();
	}

	@Override
	public String getHTML(String uuid) {
		StringBuilder t = new StringBuilder();
		
		if(new ServerPagesHTML(url,user,password).exist(uuid)){
			t.append(new ServerPagesHTML(url,user,password).getPage(uuid));
		}
		
		return t.toString();
	}

	@Override
	public String getXML(String uuid) {
		StringBuilder t = new StringBuilder();
		
		if(new ServerPagesXML(url,user,password).exist(uuid)){
			t.append(new ServerPagesXML(url,user,password).getPage(uuid));
		}
		
		return t.toString();
	}

	@Override
	public String getXSLT(String uuid) {
		StringBuilder t = new StringBuilder();
		
		if(new ServerPagesXSLT(url,user,password).exist(uuid)){
			t.append(new ServerPagesXSLT(url,user,password).getPage(uuid));
		}
		
		return t.toString();
	}

	@Override
	public String getXSD(String uuid) {
		StringBuilder t = new StringBuilder();
		
		if(new ServerPagesXSD(url,user,password).exist(uuid)){
			t.append(new ServerPagesXSD(url,user,password).getPage(uuid));
		}
		
		return t.toString();
	}

	@Override
	public String getXSDAssociated(String uuid) {
		StringBuilder t = new StringBuilder();
		
		if(new ServerPagesXSLT(url,user,password).exist(uuid)){
			t.append(new ServerPagesXSLT(url,user,password).getXsd(uuid));
		}
		
		return t.toString();
	}

}
