package es.uvigo.esei.dai.hybridserver;

import javax.jws.WebMethod;
import javax.jws.WebService;

@WebService
public interface ServerSource {
	
	@WebMethod
	public String getHTMLList();
	
	@WebMethod
	public String getXMLList();
	
	@WebMethod
	public String getXSDList();
	
	@WebMethod
	public String getXSLTList();

	@WebMethod
	public String getHTML(String uuid);
	
	@WebMethod
	public String getXML(String uuid);
	
	@WebMethod
	public String getXSLT(String uuid);
	
	@WebMethod
	public String getXSD(String uuid);
	
	@WebMethod
	public String getXSDAssociated(String uuid);
}
