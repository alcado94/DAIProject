package es.uvigo.esei.dai.hybridserver;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;
import java.net.MalformedURLException;
import java.net.Socket;
import java.net.URL;
import java.util.List;
import java.util.Map;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;

import es.uvigo.esei.dai.hybridserver.Controllers.BaseController;
import es.uvigo.esei.dai.hybridserver.Controllers.ControllerHTML;
import es.uvigo.esei.dai.hybridserver.Controllers.ControllerXML;
import es.uvigo.esei.dai.hybridserver.Controllers.ControllerXSD;
import es.uvigo.esei.dai.hybridserver.Controllers.ControllerXSLT;
import es.uvigo.esei.dai.hybridserver.http.HTTPHeaders;
import es.uvigo.esei.dai.hybridserver.http.HTTPParseException;
import es.uvigo.esei.dai.hybridserver.http.HTTPRequest;
import es.uvigo.esei.dai.hybridserver.http.HTTPRequestMethod;
import es.uvigo.esei.dai.hybridserver.http.HTTPResponse;
import es.uvigo.esei.dai.hybridserver.http.HTTPResponseStatus;
import es.uvigo.esei.dai.hybridserver.http.MIME;

public class Servicio implements Runnable{
	
	private Socket socket;
	private String url;
	private String user;
	private String password;
	private List<ServerConfiguration> servers;

	public Servicio(Socket socket,String url,String user,String password, List<ServerConfiguration> servers) {
		this.socket = socket;
		this.url = url;
		this.user = user;
		this.password = password;
		this.servers = servers;
	}
	

	public HTTPResponse init(HTTPResponse res){
		
		String index = "<h1>Hybrid Server</h1> "
				+ "<h2>Alejandro Campos Dominguez, Fernando Campos Tato</h2> ";
		
		res.setContent(index);
		res.setStatus(HTTPResponseStatus.S200);
		res.setVersion(HTTPHeaders.HTTP_1_1.getHeader());
		res.putParameter(HTTPHeaders.CONTENT_TYPE.getHeader(), MIME.TEXT_HTML.getMime());
		res.putParameter(HTTPHeaders.CONTENT_LENGTH.getHeader(), Integer.toString(index.length()));
		
		return res;
		
	}
	
	public HTTPResponse getInvalidPage(HTTPResponse res) {
		
		res.setStatus(HTTPResponseStatus.S404);
		res.setVersion(HTTPHeaders.HTTP_1_1.getHeader());
		res.putParameter(HTTPHeaders.CONTENT_TYPE.getHeader(), MIME.TEXT_HTML.getMime());
		res.setContent(res.getStatus() + " - " + res.getStatus().getStatus());
		
		return res;
	}
	
	public HTTPResponse showError(HTTPResponse res, HTTPResponseStatus http) {
		
		
		res.setStatus(http);
		res.setVersion(HTTPHeaders.HTTP_1_1.getHeader());
		res.putParameter(HTTPHeaders.CONTENT_TYPE.getHeader(), MIME.TEXT_HTML.getMime());
		res.setContent(res.getStatus() + " - " + res.getStatus().getStatus());
		
		return res;
		
	}
	
	private static final String[] routes = new String[]{
			"html",
			"xml",
			"xsd",
			"xslt"
	};
	
	private static final String[] routesPost = new String[]{
			"html",
			"xml",
			"xsd",
			"xslt"
	};
	
	public boolean haveRoute(HTTPRequest sol){
		
		boolean toret = false;
		
		for (String route : routes) {
			if(sol.getResourceName().equals(route)){
				toret=true;
			}
		}
		
		return toret;
		
	}

	@Override
	public void run() {
		
		BaseController se = null;
		
		try(Socket socket = this.socket) {
			OutputStream out =  socket.getOutputStream();
			InputStream in = socket.getInputStream();
			
			//Leer peticion
			Reader reader =  new InputStreamReader(in);
		
			HTTPResponse res = new HTTPResponse();

			HTTPRequest sol = new HTTPRequest(reader);

			Map<String, String> para = sol.getResourceParameters();
			
			String[] path = sol.getResourcePath();

			try {
				
				if(path.length != 0){
					switch(path[0]) {
					
					case "html": se = new ControllerHTML(url,user,password,servers);
								 break;
					case "xml" : se = new ControllerXML(url,user,password,servers);
								 break;
					case "xsd" : se = new ControllerXSD(url,user,password,servers);
					 			 break;
					case "xslt" : se = new ControllerXSLT(url,user,password,servers);
					 			 break;
					case ""    : res = init(res);
					 			 break;
					default : res = showError(res, HTTPResponseStatus.S400);
								break;					
					}
				}else{
					res = init(res);
				}
				
				System.out.println("Tipo solicitud : " + sol.getMethod() + "\n");
				if(se != null){
					
					if(sol.getMethod().equals(HTTPRequestMethod.GET)) {				
						
						if(haveRoute(sol)){
							
							if(sol.getResourceName().equals("xml") && para.containsKey("xslt") && para.containsKey("uuid")){
							
								res = ((ControllerXML)se).getConverted(res, para);		
								
							}else if(sol.getResourceName().equals("xml") && para.containsKey("xslt") && para.containsKey("uuid") && ((ControllerXML)se).checkUUIDXSLT(para)==false){
								
								res = showError(res, HTTPResponseStatus.S404);
								
							}else if(haveRoute(sol) && para.size()==1 && para.containsKey("uuid") && para.get("uuid")!=""){
								System.out.println(para.get("uuid"));System.out.println("hola");
								res = se.getPage(res, para);
								
							}else if(haveRoute(sol) && para.size()>1) {
								
								res= showError(res, HTTPResponseStatus.S400);
								
							}else if(para.isEmpty()){
								
								res = se.getList(res);
							
							}else if(se.checkcon()){
								
								res = getInvalidPage(res);
								
							}else {
								
								res = showError(res, HTTPResponseStatus.S500);
								
							}
						}else{
						
							res= showError(res, HTTPResponseStatus.S400);
						
						}
						
					}else if(sol.getMethod().equals(HTTPRequestMethod.POST)) {
						
						if(haveRoute(sol) && 
								(para.containsKey("html") || para.containsKey("xml") || 
										para.containsKey("xsd") || para.containsKey("xslt"))) {
		
							res = se.postPage(res,sol, para);
						
						}else if(se.checkcon()){
							
							res = showError(res, HTTPResponseStatus.S400);
							
						}else {
							
							res = showError(res, HTTPResponseStatus.S500);
							
						}
						
						
					}else if(sol.getMethod().equals(HTTPRequestMethod.DELETE)){
						
						if(haveRoute(sol)){
						
							if(se.checkUUID(para)) {
								
								res = se.delete(res,para);
								
							}else if(se.checkcon() && para.size()==1){
								
								res = showError(res, HTTPResponseStatus.S404); 
								
							}else if(se.checkcon() && para.size()>1){
								
								res = showError(res, HTTPResponseStatus.S400); 
								
							}else if(se.checkcon() && para.size()==0){
								
								res = showError(res, HTTPResponseStatus.S400);
							
							}else {
								res = showError(res, HTTPResponseStatus.S500);
							}
						
						}else{
							
							res= showError(res, HTTPResponseStatus.S400);
						
						}
						
					}
					else{
						
						res = showError(res, HTTPResponseStatus.S400);
					
					}
	
				}
			} catch (Exception e) {

				res.setStatus(HTTPResponseStatus.S500);
				res.setVersion(HTTPHeaders.HTTP_1_1.getHeader());
				res.setContent(res.getStatus() + " - " + res.getStatus().getStatus());

			}
			
			// Responder al cliente
			Writer writer = new OutputStreamWriter(out);
			res.print(writer);

		} catch (IOException e) {

			e.printStackTrace();
		} catch (HTTPParseException e) {

			e.printStackTrace();
		}
		
	}
	
	

}
