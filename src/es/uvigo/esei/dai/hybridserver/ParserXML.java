package es.uvigo.esei.dai.hybridserver;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class ParserXML {
	
	public static Document loadAndValidateWithExternalXSD( String documentPath, String schemaPath ) 
			throws ParserConfigurationException, SAXException, IOException { 
		
		// Construcción del schema 
		SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI); 
		Schema schema = schemaFactory.newSchema(new File(schemaPath));
		
		// Construcción del parser del documento. Se establece el esquema y se 
		// activa la validación y comprobación de namespaces 
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance(); 
		factory.setValidating(false); 
		factory.setNamespaceAware(true); 
		factory.setSchema(schema);
		
		// Se añade el manejador de errores 
		DocumentBuilder builder = factory.newDocumentBuilder(); 
		builder.setErrorHandler(new SimpleErrorHandler());
		
		return builder.parse(new File(documentPath));
	}

	public static boolean validateAgainstXSD(String xml, String xsd)
	{
	    try
	    {
	        SchemaFactory factory = 
	            SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
	        Schema schema = factory.newSchema(new StreamSource((new StringReader(xsd))));
	        Validator validator = schema.newValidator();
	        validator.validate(new StreamSource(new StringReader(xml)));
	        return true;
	    }
	    catch(Exception ex)
	    {
	        return false;
	    }
	}
}
