package es.uvigo.esei.dai.hybridserver;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.URL;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import javax.jws.WebService;
import javax.xml.namespace.QName;
import javax.xml.ws.Endpoint;
import javax.xml.ws.Service;

import es.uvigo.esei.dai.hybridserver.DAO.ServerPagesAbstract;
import es.uvigo.esei.dai.hybridserver.DAO.ServerPagesHTML;
import es.uvigo.esei.dai.hybridserver.DAO.ServerPagesMap;


public class HybridServer {
	
	//private static final int SERVICE_PORT = 8888;
	private Thread serverThread;
	private ExecutorService exe;
	private boolean stop;

	//private ServerPagesAbstract biblio;

	//private Map<String, String> pages;

	private static Configuration config;
	private Properties prop;
	private int puerto;
	private int clientes;
	private String url;

	private String user;
	private String password;
	private List<ServerConfiguration> servers;
	private String service;
	//private List<ServerSource> services;
	
	private Endpoint endpoint;

	public HybridServer() {

		prop = new Properties();
		prop.setProperty("numClients", "50");
		prop.setProperty("port", "8888");
		prop.setProperty("db.url", "jdbc:mysql://localhost:3306/hstestdb");
		prop.setProperty("db.user", "hsdb");
		prop.setProperty("db.password", "hsdbpass");

		
		url = prop.getProperty("db.url");
		user = prop.getProperty("db.user");
		password = prop.getProperty("db.password");

		//biblio = new ServerPagesHTML(url,user,password);

		puerto = Integer.parseInt(prop.getProperty("port"));
		clientes = Integer.parseInt(prop.getProperty("numClients"));
		service = null;
		servers = null;
	}
	
	public HybridServer(Configuration config) throws MalformedURLException {

		this.url = config.getDbURL();
		this.user = config.getDbUser();
		this.password = config.getDbPassword();
		//this.config = config;
		this.puerto = config.getHttpPort();
		this.clientes = config.getNumClients();
		this.servers = config.getServers();
		this.service = config.getWebServiceURL();
	}

	

	public HybridServer(Properties properties) {

		this.prop = properties;
		
		url = prop.getProperty("db.url");
		user = prop.getProperty("db.user");
		password = prop.getProperty("db.password");

		puerto = Integer.parseInt(prop.getProperty("port"));
		clientes = Integer.parseInt(prop.getProperty("numClients"));
		servers = null;
		service = null;
	}
	

	public int getPort() {
		return puerto;
	}

	public void start() {
		
		if(service!=null){
			endpoint = Endpoint.publish(service, new HybridServerService(url,user,password));
		
			System.out.println("Publishing Server " + service + " on port " + puerto + "\n");
		}
		
		this.serverThread = new Thread() {
			@Override
			public void run() {
				try (final ServerSocket serverSocket = new ServerSocket(getPort())) {
					
					exe = Executors
							.newFixedThreadPool(clientes);

					if(endpoint!=null)
						endpoint.setExecutor(exe);
					
					while (true) {
						
						Socket socket = serverSocket.accept();
						
						System.out.println("Solicutid al servidor: " + url);
						
						if (stop) break;
							
						exe.execute(new Servicio(socket,url,user,password,servers));
						
					}

				} catch (IOException e) {
					e.printStackTrace();
				}
				
			}
		};

		this.stop = false;
		this.serverThread.start();
		
	}

	public void stop() {
		this.stop = true;
		if(service!=null)
			this.endpoint.stop();
		
		try (Socket socket = new Socket("localhost", getPort())) {
			// Esta conexión se hace, simplemente, para "despertar" el hilo servidor			
		} catch (IOException e) {
			throw new RuntimeException(e);
		}

		try {
			
			this.serverThread.join();
		} catch (InterruptedException e) {
			throw new RuntimeException(e);
		}
		
		exe.shutdownNow();
		try {
			exe.awaitTermination(Long.MAX_VALUE, TimeUnit.DAYS);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		this.serverThread = null;
	}
	

	
}
