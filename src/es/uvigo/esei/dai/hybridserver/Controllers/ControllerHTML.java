package es.uvigo.esei.dai.hybridserver.Controllers;


import java.io.IOException;
import java.net.MalformedURLException;
import java.net.Socket;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import es.uvigo.esei.dai.hybridserver.HybridServer;
import es.uvigo.esei.dai.hybridserver.ServerConfiguration;
import es.uvigo.esei.dai.hybridserver.ServerSource;
import es.uvigo.esei.dai.hybridserver.DAO.ServerPagesHTML;
import es.uvigo.esei.dai.hybridserver.DAO.ServerPagesXSLT;
import es.uvigo.esei.dai.hybridserver.http.HTTPHeaders;
import es.uvigo.esei.dai.hybridserver.http.HTTPRequest;
import es.uvigo.esei.dai.hybridserver.http.HTTPRequestMethod;
import es.uvigo.esei.dai.hybridserver.http.HTTPResponse;
import es.uvigo.esei.dai.hybridserver.http.HTTPResponseStatus;
import es.uvigo.esei.dai.hybridserver.http.MIME;

import java.util.UUID;

import javax.xml.ws.Service;

public class ControllerHTML extends BaseController {

	public ControllerHTML(String url,String user,String password,List<ServerConfiguration> servers) {
		super(url,user,password,servers);
		this.biblio = new ServerPagesHTML(url,user,password);
	}
	
	@Override
	public HTTPResponse getList(HTTPResponse res) throws IOException  {
	
		res.setStatus(HTTPResponseStatus.S200);
		res.setVersion(HTTPHeaders.HTTP_1_1.getHeader());
		res.putParameter(HTTPHeaders.CONTENT_TYPE.getHeader(), MIME.TEXT_HTML.getMime());
		
		StringBuilder t = new StringBuilder();
	
		if(biblio != null){
			t.append("<h1>Local Server</h1>");
			for (String e : biblio.allPages()) {
				t.append("<a href=\"html?uuid=" + e + "\">" + e + "</a><br>");			
			}
		}
		
		t.append("</br>");
		if(this.getServers() != null){
			
			for (ServerConfiguration ser : this.getServers()) {
				
				t.append("<h2>" + ser.getName() + "</h2>");
				
				ServerSource server = getServiceWeb(ser);
				
				if(server!=null)
					t.append(server.getHTMLList());
				else
					t.append("Server Down");
				
			}
			
		}
		
		res.setContent(t.toString());
		
		return res;
		
	}

	@Override
	public HTTPResponse getPage(HTTPResponse res,Map<String, String> para) throws IOException {
		
		res.setVersion(HTTPHeaders.HTTP_1_1.getHeader());
		res.putParameter(HTTPHeaders.CONTENT_TYPE.getHeader(), MIME.TEXT_HTML.getMime());
		    
		
		if(para.get("uuid")=="") {
			
			res = showError(res, HTTPResponseStatus.S400);
			
		}else if(biblio.exist(para.get("uuid"))){
			res.setStatus(HTTPResponseStatus.S200);
			res.setContent(biblio.getPage(para.get("uuid")));
			
	    }else if(this.getServers() != null){			
	    	
			for (ServerConfiguration ser : this.getServers()) {
				ServerSource server = getServiceWeb(ser);
				if(server!=null && !server.getHTML(para.get("uuid")).equals("")){
					res.setContent(server.getHTML(para.get("uuid")));
					break;
				}
				
			}
			
			if(!res.getContent().equals("")){
				res.setStatus(HTTPResponseStatus.S200);			
			}else{
				res = getInvalidPage(res);
			}
		
		}else{
			
			res = getInvalidPage(res);
		}
		
		return res;
	}
	
	
	@Override
	public HTTPResponse postPage(HTTPResponse res,HTTPRequest sol,Map<String, String> para) {
		
		UUID t = UUID.randomUUID();
		
		String uuid = t.toString();
		
		if(para.get("html") == null) {
			res = showError(res, HTTPResponseStatus.S400);
		}
		biblio.addPage(uuid, para.get("html"));
		System.out.println(para.get("html"));
		res.setStatus(HTTPResponseStatus.S200);
		res.setVersion(HTTPHeaders.HTTP_1_1.getHeader());
		res.putParameter(HTTPHeaders.CONTENT_TYPE.getHeader(), MIME.TEXT_HTML.getMime());
		res.setContent("<a href=\"html?uuid=" + uuid + "\">" + uuid + "</a>");
		
		return res;
		
	}
	
	public HTTPResponse delete(HTTPResponse res,Map<String, String> para) {
		
		if(biblio.exist(para.get("uuid"))) {
			
			biblio.deletePage(para.get("uuid"));
			res.setStatus(HTTPResponseStatus.S200);
			res.setVersion(HTTPHeaders.HTTP_1_1.getHeader());
			res.putParameter(HTTPHeaders.CONTENT_TYPE.getHeader(), MIME.TEXT_HTML.getMime());
			res.setContent("Se ha eliminado");
			
		}else {
			
			res = getInvalidPage(res);
			
		}
		
		return res;
		
	}

	@Override
	public HTTPResponse getPagebyuuid(HTTPResponse res, String uuid) {
		
		res.setStatus(HTTPResponseStatus.S200);
		res.setVersion(HTTPHeaders.HTTP_1_1.getHeader());
		res.putParameter(HTTPHeaders.CONTENT_TYPE.getHeader(), MIME.APPLICATION_XML.getMime());
		res.setContent(biblio.getPage(uuid));
		
		return res;

	}	


}
