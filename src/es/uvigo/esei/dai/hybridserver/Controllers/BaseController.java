package es.uvigo.esei.dai.hybridserver.Controllers;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.Socket;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;

import es.uvigo.esei.dai.hybridserver.ServerConfiguration;
import es.uvigo.esei.dai.hybridserver.ServerSource;
import es.uvigo.esei.dai.hybridserver.DAO.ServerPagesAbstract;
import es.uvigo.esei.dai.hybridserver.http.HTTPHeaders;
import es.uvigo.esei.dai.hybridserver.http.HTTPRequest;
import es.uvigo.esei.dai.hybridserver.http.HTTPResponse;
import es.uvigo.esei.dai.hybridserver.http.HTTPResponseStatus;
import es.uvigo.esei.dai.hybridserver.http.MIME;

public abstract class BaseController {

	private Socket socket;
	protected ServerPagesAbstract biblio;
	protected String url;
	protected String user;
	protected String password;
	protected List<ServerConfiguration> servers;

	public List<ServerConfiguration> getServers() {
		return servers;
	}


	public BaseController(String url,String user,String password, List<ServerConfiguration> servers) {
		this.socket = socket;
		this.biblio = null;
		this.url = url;
		this.user = user;
		this.password = password;
		this.servers = servers;
	}
	
	
	public boolean checkUUID(Map<String, String> para) {
		try {
			return biblio.exist(para.get("uuid")) ? true : false;
		} catch (Exception e) {
			return true;
		}
	}
	

	public ServerSource getServiceWeb(ServerConfiguration server) throws IOException {
		
		ServerSource source = null;
		
		URL url2 = new URL(server.getWsdl());
		
        HttpURLConnection urlConn = (HttpURLConnection) url2.openConnection();
        urlConn.setConnectTimeout(200); 
        
        try {
			urlConn.connect();
			
			if (urlConn.getResponseCode() == 200) {
	        	
	        	QName name2 = new QName(server.getNamespace(),server.getService());

	    		Service service = Service.create(url2, name2);

	    		System.out.println(url2 + "--" + name2);
	    		
	    		source = service.getPort(ServerSource.class);
	    		
	        } 
		} catch (IOException e) {
			System.out.println("Server Down:" + server.getName());
		}
				
		return source;
	}
	
	public boolean checkcon() {
		return biblio.checkConnection();
	}
	
	public HTTPResponse getInvalidPage(HTTPResponse res) {
		
		res.setStatus(HTTPResponseStatus.S404);
		res.setVersion(HTTPHeaders.HTTP_1_1.getHeader());
		res.putParameter(HTTPHeaders.CONTENT_TYPE.getHeader(), MIME.TEXT_HTML.getMime());
		res.setContent(res.getStatus() + " - " + res.getStatus().getStatus());
		
		return res;
	}
	
	public HTTPResponse showError(HTTPResponse res, HTTPResponseStatus http) {
		
		
		res.setStatus(http);
		res.setVersion(HTTPHeaders.HTTP_1_1.getHeader());
		res.putParameter(HTTPHeaders.CONTENT_TYPE.getHeader(), MIME.TEXT_HTML.getMime());
		res.setContent(res.getStatus() + " - " + res.getStatus().getStatus());
		
		return res;
		
	}
	
		
	public abstract HTTPResponse getList(HTTPResponse res)
			 throws IOException;
	
	public abstract HTTPResponse getPage(HTTPResponse res,Map<String, String> para)
			 throws IOException;	
	
	public abstract HTTPResponse postPage(HTTPResponse res,HTTPRequest sol,Map<String, String> para)
			throws IOException;
	
	public abstract HTTPResponse delete(HTTPResponse res,Map<String, String> para);

	public abstract HTTPResponse getPagebyuuid(HTTPResponse res, String uuid);

	
}
