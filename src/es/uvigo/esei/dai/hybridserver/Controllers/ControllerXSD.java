package es.uvigo.esei.dai.hybridserver.Controllers;

import java.io.IOException;
import java.net.Socket;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.xml.ws.Service;

import es.uvigo.esei.dai.hybridserver.HybridServer;
import es.uvigo.esei.dai.hybridserver.ServerConfiguration;
import es.uvigo.esei.dai.hybridserver.ServerSource;
import es.uvigo.esei.dai.hybridserver.DAO.ServerPagesXML;
import es.uvigo.esei.dai.hybridserver.DAO.ServerPagesXSD;
import es.uvigo.esei.dai.hybridserver.http.HTTPHeaders;
import es.uvigo.esei.dai.hybridserver.http.HTTPRequest;
import es.uvigo.esei.dai.hybridserver.http.HTTPResponse;
import es.uvigo.esei.dai.hybridserver.http.HTTPResponseStatus;
import es.uvigo.esei.dai.hybridserver.http.MIME;

public class ControllerXSD extends BaseController {

	public ControllerXSD(String url,String user,String password,List<ServerConfiguration> servers) {
		super(url,user,password,servers);
		this.biblio = new ServerPagesXSD(url,user,password);
	}
	
	public HTTPResponse getList(HTTPResponse res) throws IOException  {
		
		res.setStatus(HTTPResponseStatus.S200);
		res.setVersion(HTTPHeaders.HTTP_1_1.getHeader());
		res.putParameter(HTTPHeaders.CONTENT_TYPE.getHeader(), MIME.TEXT_HTML.getMime());
		
		StringBuilder t = new StringBuilder();
	
		if(biblio != null){
			t.append("<h1>Local Server</h1>");
			for (String e : biblio.allPages()) {
				t.append("<a href=\"xsd?uuid=" + e + "\">" + e + "</a><br>");	
			}
		}
		
		if(this.getServers() != null){
			for (ServerConfiguration ser : this.getServers()) {
				
				t.append("<h2>" + ser.getName() + "</h2>");
				
				ServerSource server = getServiceWeb(ser);
				
				if(server!=null)
					t.append(server.getXSDList());
				else
					t.append("Server Down");
				
			}
		}
		
		res.setContent(t.toString());
		
		return res;
		
		
	}
	
	

	public HTTPResponse getPage(HTTPResponse res,Map<String, String> para) throws IOException {
			

		res.setVersion(HTTPHeaders.HTTP_1_1.getHeader());
		res.putParameter(HTTPHeaders.CONTENT_TYPE.getHeader(), MIME.APPLICATION_XML.getMime());
		    
		if(para.get("uuid")=="") {
			
			res = showError(res, HTTPResponseStatus.S400);
			
		}else if(biblio.exist(para.get("uuid"))){
			
			res.setStatus(HTTPResponseStatus.S200);
			res.setContent(biblio.getPage(para.get("uuid")));
			
		}else if(this.getServers() != null){
	    	
			for (ServerConfiguration ser : this.getServers()) {
				ServerSource server = getServiceWeb(ser);
				if(server!=null && !server.getXSD(para.get("uuid")).equals("")){
					res.setContent(server.getXSD(para.get("uuid")));
					break;
				}
			}
			
			if(!res.getContent().equals("")){
				res.setStatus(HTTPResponseStatus.S200);			
			}else{
				res = getInvalidPage(res);
			}
		
		}else{
			res = getInvalidPage(res);
		}
		
		return res;
	}
	
	
	@Override
	public HTTPResponse postPage(HTTPResponse res,HTTPRequest sol,Map<String, String> para) {
		
		UUID t = UUID.randomUUID();
		
		String uuid = t.toString();
		if(!(para.get("xsd")=="")){
			biblio.addPage(uuid, para.get("xsd"));
			
			res.setStatus(HTTPResponseStatus.S200);
			res.setVersion(HTTPHeaders.HTTP_1_1.getHeader());
			res.putParameter(HTTPHeaders.CONTENT_TYPE.getHeader(), MIME.APPLICATION_XML.getMime());
			
			res.setContent("<a href=\"xsd?uuid=" + uuid + "\">" + uuid + "</a>");
		}else{
			res = showError(res, HTTPResponseStatus.S400);
		}
		
		return res;
		
	}	
	
	
	public HTTPResponse delete(HTTPResponse res,Map<String, String> para) {
		
		if(biblio.exist(para.get("uuid"))) {
			
			biblio.deletePage(para.get("uuid"));
			res.setStatus(HTTPResponseStatus.S200);
			res.setVersion(HTTPHeaders.HTTP_1_1.getHeader());
			res.putParameter(HTTPHeaders.CONTENT_TYPE.getHeader(), MIME.APPLICATION_XML.getMime());
			res.setContent("Se ha eliminado");
			
		}else {
			
			res = getInvalidPage(res);
			
		}
		
		
		return res;
		
	}
	
	public HTTPResponse getPagebyuuid(HTTPResponse res, String uuid){
		
		res.setStatus(HTTPResponseStatus.S200);
		res.setVersion(HTTPHeaders.HTTP_1_1.getHeader());
		res.putParameter(HTTPHeaders.CONTENT_TYPE.getHeader(), MIME.APPLICATION_XML.getMime());
		res.setContent(biblio.getPage(uuid));
		
		return res;
		
	}
	

}
