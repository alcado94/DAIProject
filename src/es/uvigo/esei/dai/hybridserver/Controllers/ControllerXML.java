package es.uvigo.esei.dai.hybridserver.Controllers;


import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.net.Socket;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import es.uvigo.esei.dai.hybridserver.HybridServer;
import es.uvigo.esei.dai.hybridserver.ParserXML;
import es.uvigo.esei.dai.hybridserver.ServerConfiguration;
import es.uvigo.esei.dai.hybridserver.ServerSource;
import es.uvigo.esei.dai.hybridserver.DAO.ServerPagesAbstract;
import es.uvigo.esei.dai.hybridserver.DAO.ServerPagesAbstractXSLT;
import es.uvigo.esei.dai.hybridserver.DAO.ServerPagesXML;
import es.uvigo.esei.dai.hybridserver.DAO.ServerPagesXSD;
import es.uvigo.esei.dai.hybridserver.DAO.ServerPagesXSLT;
import es.uvigo.esei.dai.hybridserver.http.HTTPHeaders;
import es.uvigo.esei.dai.hybridserver.http.HTTPRequest;
import es.uvigo.esei.dai.hybridserver.http.HTTPRequestMethod;
import es.uvigo.esei.dai.hybridserver.http.HTTPResponse;
import es.uvigo.esei.dai.hybridserver.http.HTTPResponseStatus;
import es.uvigo.esei.dai.hybridserver.http.MIME;

import java.util.UUID;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.ws.Service;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

public class ControllerXML extends BaseController {

	public ControllerXML(String url,String user,String password,List<ServerConfiguration> servers) {
		super(url,user,password,servers);
		this.biblio = new ServerPagesXML(url,user,password);
	}

	
	public HTTPResponse getList(HTTPResponse res) throws IOException  {
	
		res.setStatus(HTTPResponseStatus.S200);
		res.setVersion(HTTPHeaders.HTTP_1_1.getHeader());
		res.putParameter(HTTPHeaders.CONTENT_TYPE.getHeader(), MIME.TEXT_HTML.getMime());
		
		StringBuilder t = new StringBuilder();
	
		if(biblio != null){
			t.append("<h1>Local Server</h1>");
			for (String e : biblio.allPages()) {

				t.append("<a href=\"xml?uuid=" + e + "\">" + e + "</a><br>");	

			}
		}
		
		if(this.getServers() != null){
			for (ServerConfiguration ser : this.getServers()) {
				
				t.append("<h1>" + ser.getName() + "</h1>");
				
				ServerSource server = getServiceWeb(ser);
				
				if(server!=null)
					t.append(server.getXMLList());
				else
					t.append("Server Down");
				
			}
		}
		
		res.setContent(t.toString());
		
		return res;
		
		
	}
	
	public HTTPResponse getPage(HTTPResponse res,Map<String, String> para) throws IOException {
			
		res.setVersion(HTTPHeaders.HTTP_1_1.getHeader());
		res.putParameter(HTTPHeaders.CONTENT_TYPE.getHeader(), MIME.APPLICATION_XML.getMime());
		    
		if(para.get("uuid")=="") {
			
			res = showError(res, HTTPResponseStatus.S400);
			
		}else if(biblio.exist(para.get("uuid"))){
			res.setStatus(HTTPResponseStatus.S200);
			res.setContent(biblio.getPage(para.get("uuid")));
		}else if(this.getServers() != null){
	    	
			for (ServerConfiguration ser : this.getServers()) {
				ServerSource server = getServiceWeb(ser);
				
				if(server!=null && !server.getXML(para.get("uuid")).equals("")){
					
					res.setContent(server.getXML(para.get("uuid")));
					break;
				}
			}
			
			if(!res.getContent().equals("")){
				res.setStatus(HTTPResponseStatus.S200);			
			}else{
				res = getInvalidPage(res);
			}
		
		}else{
			res = getInvalidPage(res);
		}
		
		return res;
	}
	
	
	
	public HTTPResponse postPage(HTTPResponse res,HTTPRequest sol,Map<String, String> para) {
		
		UUID t = UUID.randomUUID();
		
		String uuid = t.toString();
		
		if(para.get("xml") == null) {
			res = showError(res, HTTPResponseStatus.S400);
		}
		biblio.addPage(uuid, para.get("xml"));
		
		res.setStatus(HTTPResponseStatus.S200);
		res.setVersion(HTTPHeaders.HTTP_1_1.getHeader());
		res.putParameter(HTTPHeaders.CONTENT_TYPE.getHeader(), MIME.APPLICATION_XML.getMime());
		
		res.setContent("<a href=\"xml?uuid=" + uuid + "\">" + uuid + "</a>");
		
		return res;
		
	}
	
	public HTTPResponse delete(HTTPResponse res,Map<String, String> para) {
		

		
		if(biblio.exist(para.get("uuid"))) {
			
			biblio.deletePage(para.get("uuid"));
			res.setStatus(HTTPResponseStatus.S200);
			res.setVersion(HTTPHeaders.HTTP_1_1.getHeader());
			res.putParameter(HTTPHeaders.CONTENT_TYPE.getHeader(), MIME.APPLICATION_XML.getMime());
			res.setContent("Se ha eliminado");
			
		}else {
			
			res = getInvalidPage(res);
			
		}
		
		
		return res;
		
	}

		
	public boolean checkUUIDXSLT(Map<String, String> para) {
		try {
			return ((ServerPagesXML)biblio).existsXSLT(para.get("xslt")) ? true : false;
		} catch (Exception e) {
			return true;
		}
	}
	
	public HTTPResponse getConverted(HTTPResponse res,Map<String, String> para) throws ParserConfigurationException, SAXException, IOException, TransformerException{
		
		ServerPagesXSLT biblioXSLT = new ServerPagesXSLT(url,user,password);
		ServerPagesXSD biblioXSD = new ServerPagesXSD(url,user,password);
		
		res.setVersion(HTTPHeaders.HTTP_1_1.getHeader());
		res.putParameter(HTTPHeaders.CONTENT_TYPE.getHeader(), MIME.TEXT_HTML.getMime());
		
		String xml = null;
		
	    if(biblio.exist(para.get("uuid"))){
	    	
			xml = biblio.getPage(para.get("uuid"));
			
		}else if(this.getServers() != null){
	    	
			for (ServerConfiguration ser : this.getServers()) {
			
				ServerSource server = getServiceWeb(ser);
	    	
			
				if(server!=null && !server.getXML(para.get("uuid")).equals("")){
					xml = server.getXML(para.get("uuid"));
					break;
				}
					
			}
		}else{
			res = getInvalidPage(res);
			
			return res;
		}
			
		if(xml!=null){	
			
			String xslt = null;
			String xsdUUID = null;
			String xsd = null;
			
			if(biblioXSLT.exist(para.get("xslt")) && biblioXSLT.existXSD(para.get("xslt"))){
				
				xslt =  biblioXSLT.getPage(para.get("xslt"));
				xsdUUID =  biblioXSLT.getXsd(para.get("xslt"));
				xsd =  biblioXSD.getPage(xsdUUID);
				
				res = transform(res, xsd, xml, xslt);
				
			}else if(this.getServers() != null){
				
				boolean t = true;
				
				for (ServerConfiguration ser : this.getServers()) {
				
					ServerSource server = getServiceWeb(ser);
		    	
					if(server!=null && !server.getXSLT(para.get("xslt")).equals("") && 
							!server.getXSDAssociated(para.get("xslt")).equals("") && t){
						xslt = server.getXSLT(para.get("xslt"));
						xsdUUID = server.getXSDAssociated(para.get("xslt"));
						t=false;
					}
					
					
					if(server!=null && !server.getXSD(xsdUUID).equals("")){
						xsd = server.getXSD(xsdUUID);
						break;
					}
				
					
				}
				
				
				if(xsd!=null && xslt !=null)
					res = transform(res, xsd, xml, xslt);
				else{
					
					res = getInvalidPage(res);
				}
			}else{
				res = getInvalidPage(res);
			}
		}else{
			res = getInvalidPage(res);
		}
	
		
	    return res;
				
	}
	
	
	public HTTPResponse transform(HTTPResponse res, String xsd, String xml, String xslt) throws TransformerException{
		
		StreamSource f = new StreamSource(new StringReader(xsd));

		if(ParserXML.validateAgainstXSD(xml,xsd)){
			TransformerFactory tFactory = TransformerFactory.newInstance();
			Transformer transformer = tFactory.newTransformer(new StreamSource(new StringReader(xslt)));
			
			StringWriter writer = new StringWriter();
			
			transformer.transform(new StreamSource(new StringReader(xml)), new StreamResult(writer));

			res.setContent(writer.toString());

			res.setStatus(HTTPResponseStatus.S200);
			res.setVersion(HTTPHeaders.HTTP_1_1.getHeader());
			res.putParameter(HTTPHeaders.CONTENT_TYPE.getHeader(), MIME.TEXT_HTML.getMime());
		}else{
			res = showError(res, HTTPResponseStatus.S400);
		}
		
		return res;
		
	}



	@Override
	public HTTPResponse getPagebyuuid(HTTPResponse res, String uuid) {
		
		res.setStatus(HTTPResponseStatus.S200);
		res.setVersion(HTTPHeaders.HTTP_1_1.getHeader());
		res.putParameter(HTTPHeaders.CONTENT_TYPE.getHeader(), MIME.APPLICATION_XML.getMime());
		res.setContent(biblio.getPage(uuid));
		
		return res;
	}


	/*
	String uuidXSD = biblioXSLT.getXsd(para.get("xslt"));

	
	if(biblioXSD.exist(uuidXSD)){
		
		String xsd = biblioXSD.getPage(uuidXSD);
		
		StreamSource f = new StreamSource(new StringReader(xsd));

		if(ParserXML.validateAgainstXSD(xml,xsd)){
			TransformerFactory tFactory = TransformerFactory.newInstance();
			Transformer transformer = tFactory.newTransformer(new StreamSource(new StringReader(xslt)));
			
			StringWriter writer = new StringWriter();
			
			transformer.transform(new StreamSource(new StringReader(xml)), new StreamResult(writer));

			res.setContent(writer.toString());

			res.setStatus(HTTPResponseStatus.S200);
			res.setVersion(HTTPHeaders.HTTP_1_1.getHeader());
			res.putParameter(HTTPHeaders.CONTENT_TYPE.getHeader(), MIME.TEXT_HTML.getMime());
		}else{
			res.setStatus(HTTPResponseStatus.S400);
			res.setVersion(HTTPHeaders.HTTP_1_1.getHeader());
			res.putParameter(HTTPHeaders.CONTENT_TYPE.getHeader(), MIME.TEXT_HTML.getMime());
		}
		
	}else{
		res.setStatus(HTTPResponseStatus.S400);
		res.setVersion(HTTPHeaders.HTTP_1_1.getHeader());
		res.putParameter(HTTPHeaders.CONTENT_TYPE.getHeader(), MIME.TEXT_HTML.getMime());
	}*/
	
	

}
