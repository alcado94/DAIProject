package es.uvigo.esei.dai.hybridserver.http;

import java.io.IOException;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import com.sun.xml.internal.messaging.saaj.util.Base64;

public class HTTPRequest {
	
	String[] requestData;
	String request;
	StringBuilder content;
	private String chain;
	private String[] path;
	private HTTPRequestMethod method;
	private String name;
	private String version;
	private Map<String,String> params;
	private Map<String,String> headers;
	
	
	public HTTPRequest(Reader reader) throws  HTTPParseException {
		
		headers = new LinkedHashMap<String, String>();
		params =  new LinkedHashMap<String, String>();
		
		StringBuilder requestbuilder = new StringBuilder();
		content = new StringBuilder();
		int leido ;
		
		List<String> lines = new LinkedList<>();
		int j=0;
		while(!requestbuilder.toString().contentEquals("\r\n")){
			
			if(requestbuilder.toString().endsWith("\r\n")) {
				requestbuilder.delete(requestbuilder.length()-2, requestbuilder.length());
				lines.add(requestbuilder.toString());
				requestbuilder.delete(0, requestbuilder.length());
			}
			
			try {
				leido = reader.read();
				requestbuilder.append((char)leido);
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			
		}
		

			
			String[] values;
			for (String line : lines) {
				
				if(lines.indexOf(line) == 0) {
					values = line.split(" ");
					
					//Captura el metodo
					if(values[0].equals(HTTPRequestMethod.GET.toString()) || values[0].equals(HTTPRequestMethod.POST.toString()) || values[0].equals(HTTPRequestMethod.DELETE.toString())) {
						method = HTTPRequestMethod.valueOf(values[0]);
					}else {
						if(values[0].contains("/") || values[0].contains(HTTPHeaders.HTTP_1_1.getHeader()))
							throw new HTTPParseException("Missing method");
						else
							throw new HTTPParseException("Missing first line");
					}
					
					//Capturar el chain
					
					if(values[1].startsWith("/")) {
						chain = values[1];
						
						String[] chainarray = chain.split("\\?");
						
						//Capturar name
						name = chainarray[0];
						name = name.substring(1, name.length());
						
						//Capturar path						
						path = (getResourceName().length() > 1 ) ? getResourceName().split("/") : new String[0];
						
						//Capturar parametros para GET Y DELETE
						if(method.equals(HTTPRequestMethod.GET) || method.equals(HTTPRequestMethod.DELETE)) {
							
							if(chainarray.length>1){
								String[] param = chainarray[1].split("&");
								for(String par : param){
									
									String[] result = par.split("=");
									if(result.length == 2)
										params.put(result[0], result[1]);
								}
							}
						}
						
					}else {
						throw new HTTPParseException("Missing resource");
					}
					
					//Capturar version
					if(values.length> 2 && values[2].contains(HTTPHeaders.HTTP_1_1.getHeader())) {
						version = values[2];
					}else {
						throw new HTTPParseException("Missing version");
					}
					
					
				}
				else {
					
					//Capturar los headers
					String [] para = line.split(": ");
					
					if(para.length == 2) {
						headers.put(para[0], para[1]);
					}else {
						throw new HTTPParseException("Invalid header");
					}
				}
				
			}
			
		
		
		
		
			//Captura el contenido
			if(getHeaderParameters().containsKey(HTTPHeaders.CONTENT_LENGTH.getHeader())){
				
				for(int i=0;i<Integer.parseInt(headers.get(HTTPHeaders.CONTENT_LENGTH.getHeader()));i++){
					try {
						leido = reader.read();
						content.append((char)leido);
					} catch (IOException e) {
						e.printStackTrace();
					}
					
				}
				
				String type = getHeaderParameters().get("Content-Type");
				if (type != null && type.startsWith("application/x-www-form-urlencoded")) {
				
					try{
						StringBuilder aux = new StringBuilder();
						aux.append(URLDecoder.decode(content.toString(), "UTF-8"));
						content = aux;
						
					}catch(Exception e){
						e.printStackTrace();
					}
					
				
				}
				
			}
			else{
				content.append("");
			}
			
			
			if(method.equals(HTTPRequestMethod.POST) && getContent()!=null && getContent()!="") {
				
					String[] p = getResourceChain().split("\\?");
					
					if(p.length > 1){
						
						for(String par : p){
							
							String[] result = par.split("=");
							
							params.put(result[0],result[1]);
							
						}
						
					}
					
					String[] re = getContent().split("&");
					
					if(re.length>0){
						
						for(String par : re){
							
							String[] result = par.split("=");
							
							if(result.length != 2) {
								//throw new HTTPParseException("Error en parametros");
							}else {
								params.put(result[0],result[1]);
							}
							
							
							
						}
					}
				
			}
		
		
		
		
	}

	public HTTPRequestMethod getMethod()  {		
	
		return method;
	}

	public String getResourceChain() {
		
		return chain;
	}

	public String getResourceName() {
		
		return name;
	}
	
	public String[] getResourcePath() {
		
		return path;
	}

	public Map<String, String> getResourceParameters() {		
		
		return params;
	}

	public String getHttpVersion() {
		
		return version;
	}

	public Map<String, String> getHeaderParameters() {

		return headers;
	}

	public String getContent() {
		
		return (!content.toString().equals("")) ? content.toString() : null;
	}

	public int getContentLength() {

		return (getHeaderParameters().containsKey(HTTPHeaders.CONTENT_LENGTH.getHeader())) ? 
				Integer.parseInt(getHeaderParameters().get(HTTPHeaders.CONTENT_LENGTH.getHeader())) : 0;	
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder(this.getMethod().name()).append(' ').append(this.getResourceChain())
				.append(' ').append(this.getHttpVersion()).append("\r\n");

		for (Map.Entry<String, String> param : this.getHeaderParameters().entrySet()) {
			sb.append(param.getKey()).append(": ").append(param.getValue()).append("\r\n");
		}

		if (this.getContentLength() > 0) {
			sb.append("\r\n").append(this.getContent());
		}

		return sb.toString();
	}
}
