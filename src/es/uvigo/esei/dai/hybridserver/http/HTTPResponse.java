package es.uvigo.esei.dai.hybridserver.http;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class HTTPResponse {
	
	private HTTPResponseStatus status;
	private String version;
	private String content;
	private Map<String,String> parameters;
	
	
	public HTTPResponse() {
		
		status = null;
		version = null;
		content = "";
		parameters = new LinkedHashMap<>();
		
	}

	public HTTPResponseStatus getStatus() {
		return status;
	}

	public void setStatus(HTTPResponseStatus status) {
		this.status = status;
	}
	

	public String getVersion() {
		return this.version;
	}

	public void setVersion(String version) {
		this.version= version;
	}

	public String getContent() {
		return this.content;
	}

	public void setContent(String content) {
		this.content=content;
	}

	public Map<String, String> getParameters() {
		return this.parameters;
	}

	public String putParameter(String name, String value) {
		return this.parameters.put(name, value);
	}

	public boolean containsParameter(String name) {
		return this.parameters.containsKey(name);
	}

	public String removeParameter(String name) {
		return this.parameters.remove(name);
	}

	public void clearParameters() {
		this.parameters.clear();
	}

	public List<String> listParameters() {
		
		List<String> toret = new ArrayList<>();
		
		for (Map.Entry<String, String> entry : this.parameters .entrySet()) {
			toret.add(entry.getKey() + ": " + entry.getValue());
			
		}
		
		return toret;
	}

	public void print(Writer writer) {
		
		StringBuilder toret = new StringBuilder();
		
		
		toret.append(getVersion() + " ");
		
		toret.append(getStatus().getCode() + " " + getStatus().getStatus());

		toret.append("\r\n");
		
		for (String res : listParameters()) {
			toret.append(res + "\r\n");
		}
		
		if(!this.getContent().equals("") && !(this.getContent() == null)) {
		
			toret.append( HTTPHeaders.CONTENT_LENGTH.getHeader() + ": " + Integer.toString(this.getContent().length()));
			toret.append("\r\n");
		}
		
		toret.append("\r\n");
		toret.append(this.getContent() );
		try {
			writer.write(toret.toString());
			writer.flush();
		} catch(IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public String toString() {
		final StringWriter writer = new StringWriter();

		//try {
			this.print(writer);
		//} catch (IOException e) {
		//}

		return writer.toString();
	}
}
