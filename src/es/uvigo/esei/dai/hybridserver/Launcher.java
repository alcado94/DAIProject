package es.uvigo.esei.dai.hybridserver;


import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

public class Launcher {
	public static void main(String[] args) throws Exception {
		
			if(args.length == 1){
				
				try {
					
					XMLConfigurationLoader r = new XMLConfigurationLoader();
					Configuration config = r.load(new File(args[0]));
					
					HybridServer t = new HybridServer(config);
					
					t.start();
					
					
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} catch(IOException E) {
					E.printStackTrace();
				}				
				
			
			}else if(args.length == 0 ){
				
				HybridServer t = new HybridServer();
				t.start();
				
			}
			else{
				System.out.println("Error en configuracion");
			}
			
			
		
	}
}
