package es.uvigo.esei.dai.hybridserver.DAO;

public abstract class ServerPagesAbstractXSLT extends ServerPagesAbstract {

	public ServerPagesAbstractXSLT(String url,String user,String password) {
		super(url,user,password);
		// TODO Auto-generated constructor stub
	}
	
	public abstract boolean existXSD(String u);
	public abstract boolean existXSDinXSD(String u);
	public abstract void addPageXSLT(String k,String v,String z);
	public abstract String getXsd(String u);

}
