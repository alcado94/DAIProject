package es.uvigo.esei.dai.hybridserver.DAO;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import es.uvigo.esei.dai.hybridserver.HybridServer;


public abstract class ServerPagesAbstract {
	
	protected Map<String,String> pages;
	protected String url;
	protected String user;
	protected String pass;
	
	public ServerPagesAbstract(Map<String,String> pages){
		this.pages=pages;
	}
	
	public ServerPagesAbstract(String url,String user,String password) {
		this.url = url;
		this.user = user;
		this.pass = password;
	}
	
	public abstract boolean checkConnection();
	
	public abstract String getPage(String uuid) ;

	public abstract List<String> allPages();
	
	public abstract boolean exist(String e);	
	
	public abstract void addPage(String k,String v);
	
	public abstract void deletePage(String k);
	
	

}
