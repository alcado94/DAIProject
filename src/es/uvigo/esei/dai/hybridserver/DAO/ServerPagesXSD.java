package es.uvigo.esei.dai.hybridserver.DAO;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;

//import com.mysql.jdbc.Statement;

public class ServerPagesXSD extends ServerPagesAbstract {

	public ServerPagesXSD(String url,String user,String password) {
		super(url,user,password);
	}

	@Override
	public boolean checkConnection() {
		try(Connection connection = DriverManager.getConnection(this.url, this.user, this.pass)){
			
			return true;
		
		} catch(SQLException e) {
			e.printStackTrace();
		}
		
		return false;
	}

	@Override
	public String getPage(String uuid) {
		try(Connection connection = DriverManager.getConnection(this.url, this.user, this.pass)){
			try(PreparedStatement statement = connection.prepareStatement("Select * from XSD where uuid = ?")){
				
				statement.setString(1, uuid);
				
				try(ResultSet result = statement.executeQuery()){
					if(result.next()){
						return result.getString("content");
					}
					return null;
				}
			}
			
		} catch(SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public List<String> allPages() {
		List<String> toret = new LinkedList<>();
		
		try(Connection connection = DriverManager.getConnection(this.url,this.user,this.pass)){
			try(Statement statement = connection.createStatement()){
				try(ResultSet result = statement.executeQuery("Select uuid from XSD")){
					while(result.next()) {
						toret.add(result.getString("uuid"));
					}
					return toret;
				}
			}
		}catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public boolean exist(String e) {
		try(Connection connection = DriverManager.getConnection(this.url, this.user, this.pass)){
			
			try(PreparedStatement statement = connection.prepareStatement("Select uuid from XSD where uuid = ?")){
				
				statement.setString(1, e);
				
				try(ResultSet result = statement.executeQuery()){
					
					return result.next();
					
				}
			}
		
		} catch(SQLException E) {
			E.printStackTrace();
		}
		return false;
	}

	@Override
	public void addPage(String k, String v) {
		try(Connection connection = DriverManager.getConnection(this.url, this.user, this.pass)){
			
			try(PreparedStatement statement = connection.prepareStatement("INSERT INTO XSD (uuid, content) " 
					+  "VALUES (?, ?)")){
				
				statement.setString(1, k);
				statement.setString(2, v);
				
				int result = statement.executeUpdate();
				if(result != 1){
					throw new SQLException("Afecta a mas de una fila");
				}
				
			}
		
		} catch(SQLException e) {
			e.printStackTrace();
		}
		
	
	}

	@Override
	public void deletePage(String k) {
try(Connection connection = DriverManager.getConnection(this.url, this.user, this.pass)){
			
			try(PreparedStatement statement = connection.prepareStatement("DELETE from XSD where uuid = ?")){
				
				statement.setString(1, k);
				
				int result = statement.executeUpdate();
				
				if(result != 1){
					throw new SQLException("Afecta a mas de una fila");
				}
				
			}
		
		} catch(SQLException e) {
			e.printStackTrace();
		}
		
	}


	

}
