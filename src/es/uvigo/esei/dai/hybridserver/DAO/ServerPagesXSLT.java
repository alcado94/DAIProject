package es.uvigo.esei.dai.hybridserver.DAO;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;

public class ServerPagesXSLT extends ServerPagesAbstractXSLT{

	public ServerPagesXSLT(String url,String user,String password) {
		super(url,user,password);
	}

	@Override
	public boolean checkConnection() {
		try(Connection connection = DriverManager.getConnection(this.url, this.user, this.pass)){
			
			return true;
		
		} catch(SQLException e) {
			e.printStackTrace();
		}
		
		return false;
	}

	@Override
	public String getPage(String uuid) {
		
		try(Connection connection = DriverManager.getConnection(this.url, this.user, this.pass)){
			try(PreparedStatement statement = connection.prepareStatement("Select * from XSLT where uuid = ?")){
				
				statement.setString(1, uuid);
				
				try(ResultSet result = statement.executeQuery()){
					if(result.next()){
						return result.getString("content");
					}
					return null;
				}
			}
			
		} catch(SQLException e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	public String getXsd(String uuid) {
		try(Connection connection = DriverManager.getConnection(this.url, this.user, this.pass)){
			try(PreparedStatement statement = connection.prepareStatement("Select * from XSLT where uuid = ?")){
				
				statement.setString(1, uuid);
				
				try(ResultSet result = statement.executeQuery()){
					if(result.next()){
						return result.getString("xsd");
					}
					return null;
				}
			}
			
		} catch(SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public List<String> allPages() {
		List<String> toret = new LinkedList<>();
		
		try(Connection connection = DriverManager.getConnection(this.url, this.user, this.pass)){
			try(Statement statement = connection.createStatement()){
				
				try(ResultSet result = statement.executeQuery("Select uuid from XSLT")){
					while(result.next()){
						toret.add(result.getString("uuid"));
					}
					
					return toret;
					
				}
			}
			
		} catch(SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public boolean exist(String e) {
		try(Connection connection = DriverManager.getConnection(this.url, this.user, this.pass)){
			
			try(PreparedStatement statement = connection.prepareStatement("Select uuid from XSLT where uuid = ?")){
				
				statement.setString(1, e);
				
				try(ResultSet result = statement.executeQuery()){
					
					return result.next();
					
				}
			}
		
		} catch(SQLException E) {
			E.printStackTrace();
		}
		return false;
	}

	
	@Override
	public void addPageXSLT(String k, String v,String z) {
		try(Connection connection = DriverManager.getConnection(this.url, this.user, this.pass)){
			
			try(PreparedStatement statement = connection.prepareStatement("INSERT INTO XSLT (uuid, content,xsd) " 
					+  "VALUES (?, ?, ?)")){
				
				statement.setString(1, k);
				statement.setString(2, v);
				statement.setString(3, z);
				
				int result = statement.executeUpdate();
				if(result != 1){
					throw new SQLException("Afecta a mas de una fila");
				}
				
			}
		
		} catch(SQLException e) {
			e.printStackTrace();
		}
			
		
	}
	public void addPage(String k,String v) {
		
	}

	@Override
	public void deletePage(String k) {
		try(Connection connection = DriverManager.getConnection(this.url, this.user, this.pass)){
			
			try(PreparedStatement statement = connection.prepareStatement("DELETE from XSLT where uuid = ?")){
				
				statement.setString(1, k);
				
				int result = statement.executeUpdate();
				
				if(result != 1){
					throw new SQLException("Afecta a mas de una fila");
				}
				
			}
		
		} catch(SQLException e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public boolean existXSD(String uuid) {
		// TODO Auto-generated method stub
		try(Connection connection = DriverManager.getConnection(this.url, this.user, this.pass)){
			
			try(PreparedStatement statement = connection.prepareStatement("Select uuid from XSLT where uuid = ?")){
				
				statement.setString(1, uuid);
				
				try(ResultSet result = statement.executeQuery()){
					
					return result.next();
					
				}
			}
		
		} catch(SQLException E) {
			E.printStackTrace();
		}
		return false;
	}

	@Override
	public boolean existXSDinXSD(String uuid) {
		// TODO Auto-generated method stub
		try(Connection connection = DriverManager.getConnection(this.url, this.user, this.pass)){
			
			try(PreparedStatement statement = connection.prepareStatement("Select uuid from XSD where uuid = ?")){
				
				statement.setString(1, uuid);
				
				try(ResultSet result = statement.executeQuery()){
					
					return result.next();
					
				}
			}
		
		} catch(SQLException E) {
			E.printStackTrace();
		}
		return false;
	}
	

}
