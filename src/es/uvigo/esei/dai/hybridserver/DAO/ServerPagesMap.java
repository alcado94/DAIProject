package es.uvigo.esei.dai.hybridserver.DAO;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;


public class ServerPagesMap extends ServerPagesAbstract{
	
	
	public ServerPagesMap(Map<String,String> pages){
		super(pages);
	}
	
	
	@Override
	public String getPage(String uuid){
		return pages.get(uuid);
		
	}
	@Override
	public List<String> allPages() {
		
		List<String> toret = new LinkedList<>();
		
		for (Map.Entry<String, String> entry : pages.entrySet())
		{
		    toret.add(entry.getKey());
		}
		return toret;

	}
	@Override
	public boolean exist(String e)  {	
		return (pages.containsKey(e)) ? true : false;
	}
	@Override
	public void addPage(String k,String v)  {
		this.pages.put(k, v);
	}
	@Override
	public void deletePage(String k)  {
		
		this.pages.remove(k);
	}


	@Override
	public boolean checkConnection() {
		// TODO Auto-generated method stub
		return !pages.equals(null);
	}
	



}
