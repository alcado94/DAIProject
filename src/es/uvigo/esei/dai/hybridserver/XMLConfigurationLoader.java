/**
 *  HybridServer
 *  Copyright (C) 2017 Miguel Reboiro-Jato
 *  
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.uvigo.esei.dai.hybridserver;

import java.awt.Desktop;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;




public class XMLConfigurationLoader {
	
	public Configuration load(File xmlFile) throws Exception {
		
		// Parsear el archivo de configuracion. 
		Document doc = ParserXML.loadAndValidateWithExternalXSD(xmlFile.getPath(),"configuration.xsd");
		
		//Crear objecto de configuracion y pasar parametros
		Configuration conf = new Configuration();
		
		conf.setHttpPort(Integer.parseInt(doc.getElementsByTagName("http").item(0).getTextContent()));
		conf.setNumClients(Integer.parseInt(doc.getElementsByTagName("numClients").item(0).getTextContent()));
		conf.setWebServiceURL(doc.getElementsByTagName("webservice").item(0).getTextContent());
		
		conf.setDbUser(doc.getElementsByTagName("user").item(0).getTextContent());
		conf.setDbPassword(doc.getElementsByTagName("password").item(0).getTextContent());
		conf.setDbURL(doc.getElementsByTagName("url").item(0).getTextContent());
		
		
		//Creacion de listado de servidores disponibles
		
		List<ServerConfiguration> servers = new ArrayList<>();
		
		NodeList el = doc.getElementsByTagName("server");
		
		ServerConfiguration server;
		
		for (int i = 0; i < el.getLength(); i++) {
			
			server = new ServerConfiguration();
			
			server.setName(el.item(i).getAttributes().getNamedItem("name").getTextContent());
			server.setNamespace(el.item(i).getAttributes().getNamedItem("namespace").getTextContent());
			server.setService(el.item(i).getAttributes().getNamedItem("service").getTextContent());
			server.setWsdl(el.item(i).getAttributes().getNamedItem("wsdl").getTextContent());
			server.setHttpAddress(el.item(i).getAttributes().getNamedItem("httpAddress").getTextContent());
			
			servers.add(server);
		}
		
		conf.setServers(servers);
		
		
		return conf;
	}
	
	

	
}
