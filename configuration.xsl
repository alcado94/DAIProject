<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:r="http://www.esei.uvigo.es/dai/hybridserver">
	
	<xsl:output method="html" indent="yes" encoding="utf-8"/>
	
	
	
	<xsl:template match="/" >
		<html>
			<head>
				<title>Configuration</title>
			</head>
			<body>
				<div>
					<h1>Configuration</h1>
					<div>
						<xsl:apply-templates select="r:configuration/r:connections"/>
						<xsl:apply-templates select="r:configuration/r:database"/>
						<xsl:apply-templates select="r:configuration/r:servers"/>
					</div>
				</div>
			</body>
		</html>
	</xsl:template>
	
	<xsl:template match="r:connections">
		<div>
			<h4>Configuration</h4>
			<ul>
				<li><xsl:value-of select="r:http"/></li>
				<li><xsl:value-of select="r:webservice"/></li>
				<li><xsl:value-of select="r:numClients"/></li>
			</ul>
		</div>
	</xsl:template>
	<xsl:template match="r:database">
		<div>
			<h4>Database</h4>
			<ul>
				<li><xsl:value-of select="r:user"/></li>
				<li><xsl:value-of select="r:password"/></li>
				<li><xsl:value-of select="r:url"/></li>
			</ul>
		</div>
	</xsl:template>
	<xsl:template match="r:servers">
	
		<div>
			<h3>Servers</h3>
			
			<xsl:for-each select="r:server">
			
				<h4>Server</h4>
				<ul>
					<li><xsl:value-of select="@name"/></li>
					<li><xsl:value-of select="@wsdl"/></li>
					<li><xsl:value-of select="@namespace"/></li>
					<li><xsl:value-of select="@service"/></li>
					<li><xsl:value-of select="@httpAddress"/></li>
				</ul>
				
			</xsl:for-each>
		</div>
	</xsl:template>
	
</xsl:stylesheet>